# Overview

Wise Guy is a local SMTP server with a readline-enabled terminal 
management console.

It listens on a port for SMTP-based email. But then simply stores the email, 
without relaying it further. Thus it's mainly useful for testing.

Inspired by [Wiser](https://code.google.com/p/subethasmtp/wiki/Wiser), 
the local SMTP library.


# Build and Run

* Have a recent version of Java (the JRE should do), 7+. 6 *should* work, 
  but untested.
* Clone the repository, then build:

        cd <cloned-dir>
        ./gradlew

* Run
    
        build/install/wise-guy/bin/wise-guy[.bat]

* Type `help` for further instructions.
* Uses the awesome 
  [Gradle Application Plugin](http://www.gradle.org/docs/current/userguide/application_plugin.html), 
  if you wish to run alternative build tasks.


# Storage

* Wise Guy uses an [H2](http://www.h2database.com) embedded database for storage, 
  creating one called `wise-guy.h2.db` in the current working directory.
  

# Customisation

* To customise storage, or SMTP default ports, Java System Properties are used. 
    * The script used to run Wise Guy above has these properties, modify as needed.

  
# Limitations

* Read support limited to the `text/plain` MIME type. 
  
    * For other types, the data will be stored on receipt successfully, but attempting to 
      `view` the message will only provide the message metadata, with a placeholder message 
       for the body section indicating this limitation.
    * Instead of handling this in the app directly, it would be nice to be able to delegate it to
      some form of MIME viewer, but need to google that.
    * You can however `dump` a message to file, for further out-of-band viewing/processing.

* No formalised readline/terminal customisation hooks, e.g. history files, etc. Only 
  defaults used, handled via [jreadline](https://github.com/qmx/jreadline).


# Dev Ickiness

* No tests yet. :P


# License

Licensed under the terms of 
 [Apache Software License v2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
