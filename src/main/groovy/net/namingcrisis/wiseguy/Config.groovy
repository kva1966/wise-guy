package net.namingcrisis.wiseguy

import javax.mail.Session

class Config {
    private static final Config INSTANCE = new Config()

    public static Config getInstance() {
        INSTANCE
    }

    String getDbConnectionString() {
        "jdbc:h2:${getOr('db.file', '/tmp/wise-guy')};MV_STORE=FALSE"
    }

    String getDbUser() {
        getOr('db.user', 'sa')
    }

    String getDbPassword() {
        getOr('db.password', 'sa')
    }

    Integer getSmtpDefaultPort() {
        getOr('smtp.defaultPort', '9696').toInteger()
    }

    Session getDefaultMailSession() {
        Session.getDefaultInstance(new Properties())
    }

    String getMessageFileDir() {
        getOr('smtp.messagefileDumpDir', System.getProperty('java.io.tmpdir'))
    }

    String getSmtpUsername() {
        getOr('smtp.username', null)
    }

    String getSmtpPassword() {
        getOr('smtp.password', null)
    }

    private String getOr(String property, String defaultVal) {
        System.getProperty("wiseguy.${property}") ?: defaultVal
    }
}
