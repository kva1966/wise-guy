package net.namingcrisis.wiseguy.smtp
import groovy.transform.CompileStatic
import groovy.transform.Immutable

@Immutable
@CompileStatic
final class Result {
    boolean ok;
    String context = "";

    static Result okResult() {
        new Result(ok: true)
    }

    static Result okResult(String ctx) {
        assert ctx, "No success context specified!"
        new Result(ok: true, context: ctx)
    }

    static Result errorResult(String ctx) {
        assert ctx, "No error context specified!"
        new Result(ok: false, context: ctx)
    }

    static Result errorNoMessagesOnServer() {
        Result.errorResult("No messages on server, ignoring.")
    }

    static Result errorNoSuchMessage(id, String ctx) {
        Result.errorResult("$ctx: No message with id[$id] found.")
    }
}
