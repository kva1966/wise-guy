package net.namingcrisis.wiseguy.smtp

import net.namingcrisis.wiseguy.Config
import net.namingcrisis.wiseguy.domain.Message
import net.namingcrisis.wiseguy.domain.MessageDao
import net.namingcrisis.wiseguy.persistence.HasDbConnection
import org.codehaus.groovy.util.StringUtil
import org.subethamail.smtp.AuthenticationHandler
import org.subethamail.smtp.AuthenticationHandlerFactory
import org.subethamail.smtp.MessageListener
import org.subethamail.smtp.TooMuchDataException
import org.subethamail.smtp.auth.*
import org.subethamail.smtp.server.MessageListenerAdapter
import org.subethamail.smtp.server.SMTPServer

/**
 * {@link org.subethamail.wiser.Wiser}-inspired SMTP server with persistence hooks
 * and better state management.
 */
final class WiseGuySmtpServer implements MessageListener, SmtpServer {
    private final MessageDao messageDao
    private Integer port = Config.instance.smtpDefaultPort
    private boolean started
    private SMTPServer server

    static WiseGuySmtpServer usingDb(HasDbConnection db) {
        new WiseGuySmtpServer(db)
    }

    private WiseGuySmtpServer(HasDbConnection db) {
        messageDao = new MessageDao(db)
        messageDao.initSchema()

        initSmtpServer()
    }

    @Override
    boolean accept(String from, String recipient) {
        true
    }

    @Override
    void deliver(String from, String recipient, InputStream data) throws TooMuchDataException, IOException {
        byte[] rawData = new BufferedInputStream(data).bytes
        messageDao.save(Message.from(from, recipient, rawData))
    }

    @Override
    void start() {
        start(port)
    }

    @Override
    void start(Integer port) {
        if (!started) {
            assert port > 0 && port < 65535, "Invalid port number[$port]"
            this.port = port
            server.port = port
            server.start()
            started = true
        }
    }

    @Override
    void stop() {
        if (started) {
            server.stop()
            started = false
        }
    }

    @Override
    boolean getStarted() {
        started
    }

    @Override
    int getPort() {
        port
    }

    @Override
    Message messageWithId(Long id) {
        messageDao.findById(id)
    }

    @Override
    List<Message> getAllMessages() {
        messageDao.all()
    }

    @Override
    boolean hasMessages() {
        messageCount > 0
    }

    @Override
    Long getMessageCount() {
        messageDao.countAll()
    }

    @Override
    boolean deleteMessageWithId(Long id) {
        messageDao.delete(id)
    }

    @Override
    Long deleteAllMessages() {
        messageDao.deleteAll()
    }

    private void initSmtpServer() {
        server = new SMTPServer([this])

        String serverUsername = Config.instance.smtpUsername ?: null
        String serverPassword = Config.instance.smtpPassword ?: null;

        if (serverUsername) {
            assert serverPassword, "Supply SMTP password if username set!"
        } else {
            assert !serverPassword, "Supply SMTP username if password set!"
        }

        ((MessageListenerAdapter) server.messageHandlerFactory)
          .authenticationHandlerFactory = new AuthHandlerFactory(
          serverUsername: serverUsername,
          serverPassword: serverPassword
        )
    }


    private static class AuthHandlerFactory implements AuthenticationHandlerFactory {
        private String serverUsername
        private String serverPassword

        AuthenticationHandler create() {
            PluginAuthenticationHandler ret = new PluginAuthenticationHandler()
            UsernamePasswordValidator validator = new UsernamePasswordValidator() {
                @Override
                void login(String username, String password) throws LoginFailedException {
                    if (serverUsername) {
                        if (username != serverUsername || password != serverPassword) {
                            throw new LoginFailedException("Bad username or password")
                        }
                    }
                }
            };
            ret.addPlugin(new PlainAuthenticationHandler(validator))
            ret.addPlugin(new LoginAuthenticationHandler(validator))

            ret
        }
    }

}
