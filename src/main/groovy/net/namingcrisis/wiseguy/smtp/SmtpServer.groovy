package net.namingcrisis.wiseguy.smtp

import net.namingcrisis.wiseguy.domain.Message

/**
 * SMTP Server interface.
 */
interface SmtpServer {
    void start()
    void start(Integer port)
    void stop()
    Long getMessageCount()
    boolean hasMessages()
    boolean getStarted()
    int getPort()
    List<Message> getAllMessages()
    Message messageWithId(Long id)
    boolean deleteMessageWithId(Long id)
    Long deleteAllMessages()
}
