package net.namingcrisis.wiseguy.smtp
import groovy.transform.CompileStatic
import groovy.transform.Immutable
import net.namingcrisis.wiseguy.Config
import net.namingcrisis.wiseguy.persistence.DbAccess
import org.jboss.jreadline.complete.CompleteOperation
import org.jboss.jreadline.complete.Completion

import java.nio.file.Files
import java.nio.file.Path

final class SmtpCommands {
    private static final String ALIAS_CMD = 'alias'
    private SmtpServer server
    private final DbAccess db = new DbAccess()
    private final Closure shutdownDelegate
    private final Closure clearScreenDelegate


    private final Map<String, Command> commandMap = [
      list       : new Command(
        syntax : 'list',
        info   : 'Lists all received messages',
        handler: {
            String out = """${server.allMessages*.summaryView().join("\n")}

Found $server.messageCount message(s)."""
            Result.okResult(out)
        }
      ),

      stop       : new Command(
        syntax : 'stop',
        info   : 'Stop the mail server',
        handler: {
            if (!server.started) {
                Result.errorResult("Server not started. Ignoring.")
            } else {
                server.stop()
                Result.okResult("Server stopped.")
            }
        }
      ),

      start      : new Command(
        syntax     : 'start [port]',
        minArgCount: 0,
        maxArgCount: 1,
        info       : 'Starts the server on supplied or currently configured port.',
        handler    : { String port ->
            if (server.started) {
                Result.errorResult("Server already started. Ignoring.")
            } else {
                Integer intPort
                if (port) {
                    def p = port.isInteger() ? port.toInteger() : 0
                    if (p <= 0 || p > 65535) {
                        return Result.errorResult("Invalid port number.")
                    }
                    intPort = p
                } else {
                    intPort = server.port
                }

                server.start(intPort)
                Result.okResult("Server started on port ${server.port}")
            }
        }
      ),

      status     : new Command(
        syntax : 'status',
        info   : 'Returns the server status.',
        handler: {
            Result.okResult(
              "Server started[${server.started}]," +
                " port[$server.port]," +
                " message count[$server.messageCount]")
        }
      ),

      clear      : new Command(
        syntax : 'clear',
        info   : 'Clears the screen',
        handler: {
            clearScreenDelegate()
            Result.okResult()
        }
      ),

      exit       : new Command(
        syntax : 'exit',
        info   : 'Exits Wise Guy',
        handler: {
            shutdownDelegate()
        }
      ),

      view       : new Command(
        syntax     : 'view <message id>',
        info       : 'Opens the message with the given id for viewing.',
        minArgCount: 1,
        maxArgCount: 1,
        handler    : { String identifier ->
            if (!server.hasMessages()) {
                return Result.errorNoMessagesOnServer()
            }

            CommandUtils.withId(identifier) { Long id ->
                def msg = server.messageWithId(id)
                msg ? Result.okResult(msg.fullView()) : Result.errorNoSuchMessage(id, 'View')
            }
        }
      ),

      dump       : new Command(
        syntax     : 'dump <message id>',
        info       : 'Dumps the message with the given id to file.',
        minArgCount: 1,
        maxArgCount: 1,
        handler    : { String identifier ->
            if (!server.hasMessages()) {
                return Result.errorNoMessagesOnServer()
            }

            File dir = new File(Config.instance.messageFileDir)

            if (!dir.isDirectory()) {
                return Result.errorResult("[$dir] expected to be a directory")
            }
            if (!dir.canWrite()) {
                return Result.errorResult("[$dir] is not writeable")
            }

            CommandUtils.withId(identifier) { Long id ->
                def msg = server.messageWithId(id)
                if (!msg) {
                    return Result.errorNoSuchMessage(id, 'Dump')
                }
                Path fpath = Files.createTempFile(dir.toPath(), 'wise-guy', "$id")
                Files.write(fpath, msg.rawData)
                return Result.okResult("Message dumped to $fpath.")
            }
        }
      ),

      delete       : new Command(
        syntax     : 'delete <message id>|all',
        info       : 'Deletes the message with the given id, or all messages.',
        minArgCount: 1,
        maxArgCount: 1,
        handler    : { String identifier ->
            if (!server.hasMessages()) {
                return Result.errorNoMessagesOnServer()
            }

            if (identifier?.trim() == 'all') {
                Long count = server.deleteAllMessages()
                Result.okResult("Deleted $count messages.")
            } else {
                CommandUtils.withId(identifier) { Long id ->
                    boolean deleted = server.deleteMessageWithId(id)
                    deleted ? Result.okResult('Message deleted.') : Result.errorNoSuchMessage(id, 'Delete')
                }
            }
        }
      ),

      (ALIAS_CMD): new Command(
        syntax : "$ALIAS_CMD <key>=<value>",
        info   : "readline aliases for Wise Guy commands, e.g. $ALIAS_CMD c=clear",
        handler: {
            throw new RuntimeException("Expected to be handled by the readline impl!")
        }
      )
    ]

    static SmtpCommands withDelegates(Closure clearScreenDelegate, Closure shutDownDelegate) {
        new SmtpCommands(clearScreenDelegate, shutDownDelegate)
    }

    private SmtpCommands(Closure clsDlgt, Closure shutdownDlgt) {
        assert clsDlgt
        assert shutdownDlgt

        db.init()
        clearScreenDelegate = clsDlgt
        shutdownDelegate = {
            db.shutdown()
            shutdownDlgt()
        }
        server = WiseGuySmtpServer.usingDb(db)

        configureHelpCommand()

    }

    boolean hasCommand(String command) {
        commandMap.containsKey(command)
    }

    Command getAt(String commandName) {
        commandMap[commandName]
    }

    Integer serverPort() {
        server.port
    }

    void serverShutdown() {
        if (server.started) {
            server.stop()
        }
    }

    void serverStart() {
        assert !server.started
        server.start()
    }

    @CompileStatic
    Completion getCompletionHandler() {
        new Completion() {
            def commands = commandMap.keySet().findAll {
                // remove alias from command list, handled by readline impl.
                it != ALIAS_CMD
            } as List

            def findCandidates = { String buff ->
                commands.findAll { it.startsWith(buff) }
            }

            @Override
            void complete(CompleteOperation op) {
                // never add 'alias' as a candidate, handled by the console.
                op.addCompletionCandidates(findCandidates(op.buffer.trim()))
            }
        }
    }

    private void configureHelpCommand() {
        String helpKey = 'help'
        String helpSyntax = helpKey
        assert !commandMap.containsKey(helpKey)

        // calculate largest syntax string length, for use in padding
        int maxChars = (commandMap.values()*.syntax << helpSyntax)*.length().max()

        String helpInfo = 'Prints available commands and their use.'
        Closure<String> fmt = { String syntax, String usage ->
            "${syntax.padRight(maxChars)} - $usage"
        }
        String helpMsg = (commandMap.collect { k, v ->
            fmt(v.syntax, v.info)
        } << fmt(helpSyntax, helpInfo)).sort().join("\n")

        def helpResult = Result.okResult(helpMsg)

        commandMap[(helpKey)] = new Command(
          syntax : helpSyntax,
          info  : helpInfo,
          handler: {
              helpResult
          }
        )
    }

    @Immutable
    static final class Command {
        String syntax
        Closure<Result> handler
        String info
        int minArgCount = 0
        int maxArgCount = 0
    }
}
