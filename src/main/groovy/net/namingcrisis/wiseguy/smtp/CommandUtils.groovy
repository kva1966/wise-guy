package net.namingcrisis.wiseguy.smtp

import groovy.transform.PackageScope

@PackageScope
final class CommandUtils {
    static Result withId(String unparsedId, Closure<Result> closure) {
        String ident = unparsedId?.trim()

        def id = ident == null || !ident.isLong() ? -1 : ident.toLong()
        if (id < 0) {
            return Result.errorResult("Invalid id format.")
        }

        closure(id)
    }
}
