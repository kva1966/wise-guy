package net.namingcrisis.wiseguy.persistence

interface HasDbConnection {
    /**
     * @param closure passed in a DB connection as an argument.
     */
    def <R> R withConnection(Closure<R> operation)
}