package net.namingcrisis.wiseguy.persistence

import net.namingcrisis.wiseguy.Config
import org.h2.jdbcx.JdbcConnectionPool

import java.sql.Connection

/**
 * Stateful DB access class.
 */
class DbAccess implements HasDbConnection {
    private static final Config config = Config.instance
    private static final String  DB = config.dbConnectionString
    private static final String DB_USER = config.dbUser
    private static final String DB_PASSWORD = config.dbPassword

    private JdbcConnectionPool pool
    private boolean initialised

    /**
     * Initialises the DB
     */
    void init() {
        assert !initialised, "Already initialised!"
        initPool()
        assert initialised
    }

    /**
     * @param closure passed in a DB connection as an argument.
     */
    def <R> R withConnection(Closure<R> operation) {
        assert initialised, "Call init() first!"

        Connection conn = pool.connection
        try {
            operation(conn)
        } finally {
            conn.close()
        }
    }

    void shutdown() {
        if (initialised) {
            pool.dispose()
            initialised = false
        }
    }

    private void initPool() {
        pool = JdbcConnectionPool.create(DB, DB_USER, DB_PASSWORD)
        assert pool
        initialised = true
    }

}
