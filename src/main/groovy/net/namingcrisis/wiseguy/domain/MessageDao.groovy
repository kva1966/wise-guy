package net.namingcrisis.wiseguy.domain

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.transform.CompileStatic
import net.namingcrisis.wiseguy.persistence.HasDbConnection

import java.sql.Blob
import java.sql.Clob
import java.sql.Connection

@CompileStatic
class MessageDao {
    private final HasDbConnection db

    MessageDao(HasDbConnection connection) {
        assert connection
        db = connection
    }

    List<Message> all() {
        withSql { Sql sql ->
            String selectAll = '''
select * from message
'''
            sql.rows(selectAll).collect {
                toMessage(it)
            } as List
        }
    }


    Message findById(Long id) {
        withSql { Sql sql ->
            String q = '''
select * from message where id = :id
'''
            List<GroovyRowResult> res = sql.rows(q, id: id)
            assert res.size() <= 1, "Non-unique id[$id] for messages! Bad schema!"

            if (!res) {
                null as Message
            } else {
                toMessage(res.first())
            }
        }
    }

    boolean delete(Long id) {
        withSql { Sql sql ->
            String q = '''
delete from message where id = :id
'''
            int deleteCount = sql.executeUpdate(q, id: id)
            assert deleteCount <= 1
            deleteCount == 1
        }
    }

    int deleteAll() {
        withSql { Sql sql ->
            String q = '''
delete from message
'''
            sql.executeUpdate(q)
        }
    }

    Message save(Message message) {
        withSql { Sql sql ->
            String idValQ = 'select seq_message.nextval as idval'
            def insertQ = '''
insert into message(
    id,
    sender,
    to_receivers,
    cc_receivers,
    bcc_receivers,
    received_date,
    subject,
    raw_data,
    mime_type
) values (
    :id,
    :sender,
    :to,
    :cc,
    :bcc,
    :receivedDate,
    :subject,
    :rawData,
    :mimeType
)
'''
            Long id = sql.firstRow(idValQ).idval as Long

            sql.executeInsert(insertQ,
              id: id,
              sender: message.from,
              to: message.to.join(","),
              cc: message.cc.join(","),
              bcc: message.bcc.join(","),
              receivedDate: message.date,
              subject: message.subject,
              rawData: message.rawData,
              mimeType: message.mimeType
            )

            Message.from(id, message)
        }
    }

    Long countAll() {
        withSql { Sql sql ->
            String q = '''
select count(id) as message_count from message
'''
            sql.firstRow(q).message_count as Long
        }
    }

    void initSchema() {
        withSql { Sql sql ->
            String q = '''
create sequence if not exists seq_message
start with 1
increment by 1
minvalue 1
no maxvalue
no cycle
no cache;

create table if not exists message (
    id bigint default seq_message.nextval primary key,
    sender varchar not null,
    to_receivers clob not null,
    cc_receivers clob,
    bcc_receivers clob,
    received_date varchar,
    subject varchar,
    raw_data blob not null,
    mime_type varchar
);
'''
            sql.execute(q)
        }
    }

    private <R> R withSql(Closure<R> closure) {
        db.withConnection { Connection conn ->
            def sql = new Sql(conn)
            closure(sql)
        }
    }

    private Message toMessage(GroovyRowResult row) {
        Closure<List<String>> clob2list = { String property ->
            (row[(property)] as Clob)?.characterStream.text.split(',') as List
        }
        Blob rawDataBlob = row.raw_data as Blob
        byte[] rawData = rawDataBlob ? rawDataBlob.binaryStream.bytes : null

        new Message(
          id: row.id,
          from: row.sender,
          to: clob2list('to_receivers'),
          cc: clob2list('cc_receivers'),
          bcc: clob2list('bcc_receivers'),
          date: row.received_date,
          subject: row.subject,
          mimeType: row.mime_type,
          rawData: rawData
        )
    }
}
