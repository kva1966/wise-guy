package net.namingcrisis.wiseguy.domain

import groovy.transform.CompileStatic
import groovy.transform.Immutable
import net.namingcrisis.wiseguy.Config

import javax.mail.internet.MimeMessage

/**
 * Represents a single email message.
 */
@CompileStatic
@Immutable
class Message {
    Long id
    String from
    List<String> to = []
    List<String> cc = []
    List<String> bcc = []
    String date
    String subject
    byte[] rawData
    String mimeType

    static Message from(Long id, Message src) {
        new Message(
          id: id,
          from: src.from.collect(),
          to: src.to.collect(),
          cc: src.cc.collect(),
          bcc: src.bcc.collect(),
          subject: src.subject,
          date: src.date,
          mimeType: src.mimeType,
          rawData: new ByteArrayInputStream(src.rawData).bytes,
        )
    }

    static Message from(String from, String to, byte[] rawData) {
        MimeMessage mimeMessage = getMimeMessage(rawData)
        String mimeType = mimeType(mimeMessage)

        new Message(
          from: from,
          to: [to],
          subject: mimeMessage.subject,
          date: mimeMessage.getHeader('Date')[0],
          mimeType: mimeType,
          rawData: new ByteArrayInputStream(rawData).bytes // copy.
        )
    }

    String summaryView() {
        "$id | $date | $from | ${to[0]} | $subject | $mimeType"
    }

    String fullView() {
        """---
From:    ${from}
To:      ${to.join(',')}
Date:    $date
CC:      ${cc.join(',')}
BCC:     ${bcc.join(',')}
Subject: ${subject ?: ''}

${body ?: ''}
---"""
    }

    String getBody() {
        mimeType.contains("text/plain") ? asMimeMessage().content
          : "<Wise Guy ain't so wise, $mimeType unsupported for viewing>"
    }

    MimeMessage asMimeMessage() {
        getMimeMessage(rawData)
    }

    static MimeMessage getMimeMessage(byte[] rawData) {
        new MimeMessage(Config.instance.defaultMailSession, new ByteArrayInputStream(rawData));
    }

    static String mimeType(MimeMessage msg) {
        msg.getHeader('Content-Type')[0]
    }
}
