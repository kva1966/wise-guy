package net.namingcrisis.wiseguy.ui


/**
 * wise-guy-specific decorator around {@link org.jboss.jreadline.console.Console}.
 */
final class WiseConsole {
    static final String PROMPT = "[${clr(GREEN_ON_BLACK, 'wise-guy')}] "

    // http://bitmote.com/index.php?post/2012/11/19/Using-ANSI-Color-Codes-to-Colorize-Your-Bash-Prompt-on-Linux
    // http://www.linuxfocus.org/English/May2004/article335.shtml
    static final String SHELL_DEFAULT = "0m"
    static final String GREEN_ON_BLACK = "0;32;40m"
    static final String RED_ON_BLACK = "0;31;40m"
    static final String BOLD_BLUE_ON_BLACK = "1;34;40m"
    static final String BOLD_WHITE_ON_BLACK = "1;37;40m"

    @Delegate(interfaces = false)
    private org.jboss.jreadline.console.Console console

    void writeln(String s) {
        console.pushToStdOut("$s\n")
    }

    void writeWelcome() {
        writeln("${clr(BOLD_BLUE_ON_BLACK, 'Wise Guy')} at your service, fuhgeddaboudit...")
        writeln("Type ${clr(BOLD_WHITE_ON_BLACK, 'help')} to get wiser.")
    }

    void writeExit() {
        writeln(clr(BOLD_BLUE_ON_BLACK, 'Ciao, Capo.'))
    }

    void writeCmdNotFound(String cmd) {
        writeln("${clr(RED_ON_BLACK, cmd)}: Command not found. Wise guy, eh?")
    }

    void writeInvalidSyntax(String correctSyntax) {
        writeln("Usage: ${clr(BOLD_WHITE_ON_BLACK, correctSyntax)}. Wise guy, eh?")
    }

    void clear() {
        console.clear()
    }

    void destroy() {
        console.stop()
    }

    static String clr(String colorSeq, String text) {
        "\033[$colorSeq" + text + "\033[$SHELL_DEFAULT"
    }
}
