package net.namingcrisis.wiseguy.ui
import org.jboss.jreadline.console.Console as JrConsole
import org.jboss.jreadline.console.ConsoleOutput
/**
 * Initiates the terminal interaction.
 */
final class TerminalUi {
    JrConsole jrcons = new JrConsole()
    WiseConsole wcons = new WiseConsole(console: jrcons)
    CommandHandler cmdHandler = CommandHandler.withConsole(wcons)

    static void main(String[] args) {
        new TerminalUi().run()
    }

    private void run() {
        wcons.writeln("\n--")
        cmdHandler.init()

        ConsoleOutput line

        while ((line = jrcons.read(WiseConsole.PROMPT)) != null) {
            String buff = line.buffer.trim()

            if (!buff) {
                continue
            }

            def cmdParts = buff.split(/\s+/) as List

            // debug.
            // wcons.writeln(cmdParts.toString())
            cmdHandler.handle(cmdParts[0], *cmdParts[1..<cmdParts.size()])
        }
    }

}
