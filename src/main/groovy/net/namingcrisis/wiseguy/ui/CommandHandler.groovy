package net.namingcrisis.wiseguy.ui

import com.google.common.base.Throwables
import net.namingcrisis.wiseguy.smtp.SmtpCommands

/**
 * Handles wise guy commands.
 */
class CommandHandler {
    private final WiseConsole console
    private final SmtpCommands commands

    static CommandHandler withConsole(WiseConsole console) {
        new CommandHandler(console)
    }

    private CommandHandler(WiseConsole console) {
        assert console, "Null console provided!"
        this.console = console
        commands = SmtpCommands.withDelegates(
          { console.clear() },
          { shutdown() }
        )
        console.addCompletion(commands.getCompletionHandler())
    }

    void handle(String command, String... args) {
        if (!commands.hasCommand(command)) {
            console.writeCmdNotFound(command)
            return
        }

        try {
            SmtpCommands.Command cmd = commands[command]

            if (args.length < cmd.minArgCount || args.length > cmd.maxArgCount) {
                console.writeInvalidSyntax(cmd.syntax)
                return
            }

            def res = cmd.handler(*args)

            if (res.context) {
                console.writeln(res.context)
            }
        } catch (Exception | AssertionError e) {
            def exStr = Throwables.getStackTraceAsString(Throwables.getRootCause(e))
            console.writeln(exStr)
        }
    }

    void init() {
        console.writeln("Starting SMTP Server on port ${commands.serverPort()}")
        commands.serverStart()
        console.writeln("Started")

        console.writeWelcome()
    }

    private void shutdown() {
        console.writeln("Stopping SMTP Server")
        commands.serverShutdown()
        console.writeExit()
        console.destroy()

        System.exit(0)
    }
}
